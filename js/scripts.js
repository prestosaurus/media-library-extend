(function mediaLibraryExtend() {
  "use strict";
  Drupal.behaviors.mediaLibraryExtendBehavior = {
    attach: function attach(context) {
      mleReady();
      function mleReady() {
        var mediaItemsWrapper = document.querySelector(".media-library-views-form__rows", context);
        if (mediaItemsWrapper) {
          // This overrides a preventDefault() from media_library.click_to_select.js in media_library module.
          mediaItemsWrapper.addEventListener("click", function icClickRightBtn (e) {
            console.log("click");
            if (e.target.hasAttribute("href")) {
              window.location.href = e.target.getAttribute("href");
            }
          });
        }
      }
    }
  };
})();
