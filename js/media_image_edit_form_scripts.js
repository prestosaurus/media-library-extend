(function mediaLibraryImageFormExtend() {
  Drupal.behaviors.mediaLibraryExtendImageFormBehavior = {
    attach: function attach(context) {
      mleifReady();
      function mleifReady() {
        var imageAltTextDescription = document.getElementById("edit-field-media-image-0-alt--description", context);
        if (imageAltTextDescription) {
          imageAltTextDescription.innerHTML = '<p>' + Drupal.t('This text will be used by screen readers, search engines, or when the image cannot be loaded.<br />If the image is decorative or text surrounding the image describes it, set the alt text to <strong>"Empty"</strong>.') + '</p>';
        }
      }
    }
  };
})();
